# Componente Galeria
La idea es simular una galeria con un componente masonery y al hacer click en la imagen nos lleve a otra pagina en la que dependiendo del tipo de imagen nos mueste una información u otra en funcion del parametro pasado en la cabecera de la URL.
Dicho componente contara con 2 CSS uno para la galeria masonery y otro para la descripcion e cada imagen.

La galeria será accesible, pero la descripcion de las imagenes necesitan que se le pase en la url el id de la imagen, si se intenta acceder a la pagina descripcion.html sin selecionar ninguna imagen mostrara un error.

Para comprobar dicho error : Acceder directamente a descripcion.html sin pasar por galeria.html y selleccionar una imagen.

Hemos usado la herramienta neocities.org para poder realizar los test de accesibilidad de nuestro componente alcanzando en todos los test exito. (Adjuntamos una captura del resutlado de los test) en el directorio de cada componente. Además tambien se facilita la url de neocities para poder visualizar el componente. Es posible que la visualización desde neocities.org no sea exactamente igual en cuanto a imagenes yy videos con los archivos del .zip debido a que neocities no permite ciertas extensiones de archivos en su version gratuita (.mp4,.jfif,...)

https://componentesa17ivansv.neocities.org/ComponenteGaleria/galeria.html


#### Estructura de Directorios
ComponenteGaleria 
    
    CSS 
        componentemasonery.css
        componentedescripcion.css
        
    img
        imagenes-varias
    
    JS
        descripcion.js
        proxectos.json

    Tests
        Test1Passed.png
        Test2Passed.png

    VTT
        varios-vtt

    galeria.html
    descripcion.html
    README.md
    