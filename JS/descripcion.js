solictardatos();
//Definimos la funcion que llamara al JSON para obtener los datos necesarios
function solictardatos()
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange= function()
    {
        if(xhttp.readyState == 4 && xhttp.status == 200)
        {
            mostrar(this);
        }
    }
    let url = "./JS/proxectos.json";
    xhttp.open('GET',url,true);
    xhttp.responseType = 'json';
    xhttp.send();
}

function mostrar(json)
{
    let jsonDoc = json.response;
    let lista = jsonDoc['proxectos'];

    //Leemos el parametro que se pasa en la url, el cual nos especifica a que proyecto se esta accediendo
    let param =(location.href.split('#')[1]);
    //Ocultamos la imagen y el video hasta que comprobemos cual de los dos se debe mostrar en funcion de lo que tengamos en el campo url del JSON dependiendo de la terminacion .mp4 o .jpg
    document.getElementById('imagen').style.display="none";
    document.getElementsByTagName('video')[0].style.display="none";
    //Comprobamos si el parametro enviado coincide con el campo id de nuestro JSON, si coincide quiere decir qu deicha imagen cuenta con info, si no existe la imagen aun no cuenta con info y se mostrara un error
    var existe = null;
    var indice = 0;
    for(let i in lista)
    {
        
        if(lista[i]['id'] == param)
        {
            existe = "SI";
            indice = i
            break;
        }else
        {
            existe="NO"
        }
        
        
    }
    //Si la variable existe == SI quiere decir que nuestro proyecto si existe y contiene datos por lo que pasamos a mostrarlos modificando el DOM
    if(existe == 'SI')
    {
            document.getElementById('titulo').innerHTML = lista[indice]['titulo'];
            let pattern =/[\.]mp4$/;
            let pattern2 = /[\.]webm$/;
            //Comprobamos si nuestra url es un video en caso afirmativo ocultamo la etiquta <img> y mostramos la etiqueta <video>
            if(pattern.test(lista[indice]['url']) || pattern2.test(lista[indice]['url']))
            {
                document.getElementsByTagName('video')[0].style.display="block";
                document.getElementsByTagName('video')[0].src=lista[indice]['url'];
                document.getElementsByTagName('video')[0].setAttribute('type','video/mp4');
            }else
            {
                document.getElementById('imagen').style.display="block";
                document.getElementById('imagen').src=lista[indice]['url'];
                
            }
            document.getElementById('descripcion').innerHTML = lista[indice]['texto'];
            document.getElementsByClassName('progress2')[0].style.width = lista[indice]['estado'];
        }else
        {
                document.getElementById("seccion").style.display="none";
                function mostrarspinner()
                {
                    document.getElementById("spinner").style.display="block";
                    document.getElementById("spinner").style.opacity="1";
                }
                var mostra = setInterval(mostrarspinner,0)
                function eliminaranimacion()
                {
                    clearInterval(mostra);
                    document.getElementById("spinner").style.display="none"; 
                    document.getElementsByTagName("h1")[0].innerHTML="Error 404"
                    document.getElementsByTagName("h1")[0].style.color="black"
                    document.getElementsByTagName('body')[0].style.backgroundColor="rgb(96, 175, 138)"
                    document.getElementsByClassName('error')[0].style.display="block";
                    document.getElementsByClassName('error')[0].innerHTML="UPS! La pagina solicitada no se encuentra disponible o no dispone de lo privilegios para acceder directamente a ella. Por favor vuelva a la pagina anterior pulsando el icono que aparece a continuación.";
                    document.getElementById('retornar').classList.add('volver');

                    clearInterval(elimin)
                }
                var elimin = setInterval(eliminaranimacion,2000);
        }
    
}